%title: Suricata
%author: xavki


███████╗██╗   ██╗██████╗ ██╗ ██████╗ █████╗ ████████╗ █████╗ 
██╔════╝██║   ██║██╔══██╗██║██╔════╝██╔══██╗╚══██╔══╝██╔══██╗
███████╗██║   ██║██████╔╝██║██║     ███████║   ██║   ███████║
╚════██║██║   ██║██╔══██╗██║██║     ██╔══██║   ██║   ██╔══██║
███████║╚██████╔╝██║  ██║██║╚██████╗██║  ██║   ██║   ██║  ██║
╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# Suricata : installation

<br>

Documentation : https://docs.suricata.io/en/latest/install.html

Sur Debian : already in default packages

```
sudo add-apt-repository ppa:oisf/suricata-stable
sudo apt-get update
sudo apt-get install suricata -y
```

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : installation

<br>

Directories :

	* configuration : /etc/suricata/suricata.yml

	* merged : /var/lib/suricata/rules/suricata.rules

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : installation

<br>

* settings

```
HOME_NET:

  - eve-log:
      enabled: yes

af-packet:  # from socket or kernel memory
  - interface: eth0

pcap:  # all packet in the network
  - interface: eth0

default-rule-path: /var/lib/suricata/rules

rule-files:
  - suricata.rules
```

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : installation

<br>

* initialize rules

sudo suricata-update
