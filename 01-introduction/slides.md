%title: Suricata
%author: xavki


███████╗██╗   ██╗██████╗ ██╗ ██████╗ █████╗ ████████╗ █████╗ 
██╔════╝██║   ██║██╔══██╗██║██╔════╝██╔══██╗╚══██╔══╝██╔══██╗
███████╗██║   ██║██████╔╝██║██║     ███████║   ██║   ███████║
╚════██║██║   ██║██╔══██╗██║██║     ██╔══██║   ██║   ██╔══██║
███████║╚██████╔╝██║  ██║██║╚██████╗██║  ██║   ██║   ██║  ██║
╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝ ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝


-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

SECURITY TOOL

	* Instrusion Detection System (IDS)

	* Intrusion Prevention System (IPS)

	* Network Security Monitoring (NSM)

	* Created in 2009

	* Opensource

	* Developped in C

	* Maintain by Open Information Security Foundation (OISF)

	* Operating System : Linux, FreeBSD, OpenBSD, Windows

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

LINKS

Official website : https://suricata.io/
Github : https://github.com/OISF/suricata
Version : 7.0.3
Documentation : https://docs.suricata.io/en/latest/index.html

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

FEATURES

	* Instance : one tool install per server to send in centralized tools

	* Multithread infrastructure : high performance packet analyze

	* Rules : instructions to analyze and activate an alert

	* Signature : identify specific attacks 

	* External tool integration : like wazuh or opnsense

	* Prevention : preventive action to block attacks

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

FEATURES

	* Encrypted trafic : to inspect TLS/SSL packets

	* Updates : rules can be updated easily

	* NSM : DNS and HTTP journal, file extraction, checksum

	* Outputs : multiple output formats (txt, json...)

	* Additional scripts : can add lua scripts

	* Easy to use : with port mirroring (send all trafic)

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

STEPS

	1- Trafic capture (pcap/libpcap... or other methods)

	2- Multithread analyze : split trafic by thread

	3- Defrag IP and Assemble TCP packets to have readable trafic (application layer)

	4- Rules are applied and interpreted

	5- Deep analyze to go behind headers and check the body of packets

	6- Action and response if needed

	7- Logs are written

	8- Optional - logs are collected to be send to centralize system (ELK, SIEM...)

-----------------------------------------------------------------------------------------------------------                                          

# Suricata : introduction - what is it ?

<br>

ADD-ON

	* ANSSI : https://cyber.gouv.fr/produits-certifies/suricata-version-608
